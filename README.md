# Projet CI/CD avec Jenkins, Docker et Kubernetes pour une application Spring


## Étapes Accomplies

### 1. Référentiel de Code Source

- Un référentiel a été créé sur [GitLab](https://gitlab.com/5info2/examan-devops).
- Le référentiel est public pour faciliter l'évaluation.

### 2. Dockerisation de l'Application

- Un Dockerfile a été créé pour construire une image Docker de l'application Spring.
- L'application est fonctionnelle une fois le conteneur lancé.

### 3. Mise en Place de la Pipeline Jenkins

- Jenkins a été installé et configuré.
- Une pipeline Jenkins a été créée avec les étapes suivantes :
  - Récupération du code source depuis le référentiel GitHub/GitLab.
  - Construction de l'image Docker à partir du Dockerfile.
  - Poussée de l'image sur Docker Hub.
  - (Note: Si possible, mentionnez la mise à jour du déploiement via GitOps ici.)
  (l'algorithme de pipline est configuré dans jenkins qui contient 3 stages)

## Exécution de l'Application

Pour exécuter l'application Spring localement, suivez ces étapes :

1. **Cloner le Référentiel :**
   ```bash
   git clone [https://gitlab.com/5info2/examan-devops]
   cd [examan-devops]
   mvn spring-boot:run

